# GWS - Getränke Webshop
Ein Projekt im Rahmen des Moduls "Software Engineering III" an der Hochschule Bremerhaven
## 1. Installationsanleitung
### 1.1 Download des Projektes

    git clone git@gitlab.com:shellyger/swe3_gws.git
    git clone https://gitlab.com/shellyger/swe3_gws.git
### 1.2 Anpassen der WildFly Konfiguration
Um die hochgeladenen Bilder via HTTP bereitstellen zu können, muss das "Mapping" eingestellt werden.

 1. Öffnen Sie die Datei "standalone/configuration/standalone.xml"
 2. Suchen die Nachdem subsystem Eintrag "undertow" und passen Sie diesen an, sodass er wie folgt aussieht:

            <subsystem xmlns="urn:jboss:domain:undertow:11.0" default-server="default-server" default-virtual-host="default-host" default-servlet-container="default" default-security-domain="other" statistics-enabled="${wildfly.undertow.statistics-enabled:${wildfly.statistics-enabled:false}}">
            <buffer-cache name="default"/>
            <server name="default-server">
                <http-listener name="default" socket-binding="http" redirect-socket="https" enable-http2="true"/>
                <https-listener name="https" socket-binding="https" security-realm="ApplicationRealm" enable-http2="true"/>
                <host name="default-host" alias="localhost">
                    <location name="/" handler="welcome-content"/>
                    <location name="/gws-produkt-bilder" handler="GwsPictureDir"/>
                    <http-invoker security-realm="ApplicationRealm"/>
                </host>
            </server>
            <servlet-container name="default">
                <jsp-config/>
                <websockets/>
            </servlet-container>
            <handlers>
                <file name="welcome-content" path="${jboss.home.dir}/welcome-content"/>
                <file name="GwsPictureDir" path="${jboss.home.dir}/gws-produkt-bilder" directory-listing="true"/>
            </handlers>
        </subsystem>

Neu eingefügt wurden folgende Einträge:

    <location name="/gws-produkt-bilder" handler="GwsPictureDir"/>
    <file name="GwsPictureDir" path="${jboss.home.dir}/gws-produkt-bilder" directory-listing="true"/>

### 1.3 Erstellen des Bilder Ordners
Legen Sie in dem WildFly Home folgenden Ordner an "gws-produkt-bilder".

### 1.4 Starten des WildFlys
### 1.5 Starten der H2-Datenbank
Eventuell muss die persistence.xml nach Bedarf angepasst werden.
### 1.6 Kompilieren und Deployen

    mvn clean install wildfly:deploy
### 1.7 Aufrufen der Applikation

    http://127.0.0.1:8080/gws/

Administration:

    http://127.0.0.1:8080/gws/adminlogin.xhtml
    Benutzer: admin@admin.de
    Passwort: admin


## 2. Informationen über den Login
In der Applikation können sich Kunden und Administratoren gleichzeitig anmelden.

```de.hsb.app.gws.controller.UserSessionHandler```

Der Button zum abmelden (oben rechts) gilt immer für den aktuellen Kontext. D.h., wenn man sich in der Administration befindet, gilt dieser für die Admin-Session. Wenn man sich auf der Kundenansicht (Shop-Frontend) befindet, gilt dieser für die Kunden-Session.

