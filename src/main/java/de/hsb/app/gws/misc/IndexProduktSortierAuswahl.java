package de.hsb.app.gws.misc;

public enum IndexProduktSortierAuswahl {

	NEUSTE("Neuste"),
	NAME_ASC("Name aufsteigend"),
	NAME_DESC("Name absteigend"),
	HERSTELLER_ASC("Hersteller aufsteigend"),
	HERSTELLER_DESC("Hersteller absteigend"),
	PREIS_ASC("Preis aufsteigend"),
	PREIS_DESC("Preis absteigend");

    private String label;

    private IndexProduktSortierAuswahl(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}