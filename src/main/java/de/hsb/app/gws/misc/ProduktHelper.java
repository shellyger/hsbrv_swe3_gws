package de.hsb.app.gws.misc;

import java.util.ArrayList
;
import java.util.List;

public class ProduktHelper {

	public static final List<Integer> possible_select_quantitie = new ArrayList<Integer>();
			
	static {
		possible_select_quantitie.add(1);
		possible_select_quantitie.add(2);
		possible_select_quantitie.add(3);
		possible_select_quantitie.add(4);
		possible_select_quantitie.add(5);
		possible_select_quantitie.add(6);
		possible_select_quantitie.add(7);
		possible_select_quantitie.add(8);
		possible_select_quantitie.add(9);
		possible_select_quantitie.add(10);
	}


	public static List<Integer> getPossibleSelectQuantitie() {
		return possible_select_quantitie;
	}
}
