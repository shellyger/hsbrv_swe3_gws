package de.hsb.app.gws.misc;

public enum ProduktColumnsFuerOrder {

    ID("id"),
    NAME("name"),
    PREIS("preis"),
    HERSTELLER("hersteller")
	;

    private final String column;

    ProduktColumnsFuerOrder(final String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column;
    }
}
