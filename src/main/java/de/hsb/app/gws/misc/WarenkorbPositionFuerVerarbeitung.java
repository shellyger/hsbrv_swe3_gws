package de.hsb.app.gws.misc;

import de.hsb.app.gws.model.Produkt;

public class WarenkorbPositionFuerVerarbeitung {
	
	private Produkt produkt;
	private WarenkorbPosition warenkorbPosition;

	public WarenkorbPositionFuerVerarbeitung(Produkt produkt, WarenkorbPosition warenkorbPosition) {
		this.produkt = produkt;
		this.warenkorbPosition = warenkorbPosition;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public WarenkorbPosition getWarenkorbPosition() {
		return warenkorbPosition;
	}

	public void setWarenkorbPosition(WarenkorbPosition warenkorbPosition) {
		this.warenkorbPosition = warenkorbPosition;
	}
	
}
