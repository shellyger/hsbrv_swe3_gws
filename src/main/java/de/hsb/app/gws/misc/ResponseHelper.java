package de.hsb.app.gws.misc;

import java.io.IOException;

import javax.faces.context.FacesContext;

public class ResponseHelper {
	public static void redirectTo(String target) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(target);
	}
}
