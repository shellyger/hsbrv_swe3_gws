package de.hsb.app.gws.misc;

public enum AdminColumnsFuerOrder {

    ID("id"),
    EMAIL("email"),
    VORNAME("vorname"),
    NACHNAME("nachname")
	;

    private final String column;

    AdminColumnsFuerOrder(final String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column;
    }
}
