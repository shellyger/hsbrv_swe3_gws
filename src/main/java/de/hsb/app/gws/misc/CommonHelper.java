package de.hsb.app.gws.misc;

import java.util.List;
import java.util.stream.Collectors;

public class CommonHelper {
	
	public static String upperFirst(String text) {
		return text.substring(0,1).toUpperCase() + text.substring(1).toLowerCase();
	}
	
	/**
	 * Entfern ein Objekt aus einer Liste von Objekten und gibt anschlie�end die ersten n Eintr�ge der Liste zur�ck
	 * 
	 * @param <T>
	 * @param objektListe
	 * @param objektAusschlie�en
	 * @param n
	 * @return Liste der ersten n Objekte ohne das angegebene Objekt
	 */
	public static <T> List<T> nurDieErstenNObjekteOhneDasAngegebeneObjekt(List<T> objektListe, T objektAusschlie�en, int n) {		
		for(int i=0; i < objektListe.size(); i++) {
			if(objektListe.get(i).equals(objektAusschlie�en)) {
				objektListe.remove(i);
			}
		}
		
		return objektListe.stream().limit(n).collect(Collectors.toList());
	}
}
