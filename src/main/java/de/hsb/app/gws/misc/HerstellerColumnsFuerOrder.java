package de.hsb.app.gws.misc;

public enum HerstellerColumnsFuerOrder {

    ID("id"),
    NAME("name")
	;

    private final String column;

    HerstellerColumnsFuerOrder(final String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column;
    }
}
