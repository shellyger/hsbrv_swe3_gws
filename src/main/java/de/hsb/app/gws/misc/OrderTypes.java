package de.hsb.app.gws.misc;

public enum OrderTypes {

    ASC("ASC"),
    DESC("DESC")
	;

    private final String type;

    OrderTypes(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
