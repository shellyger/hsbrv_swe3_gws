package de.hsb.app.gws.misc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.Instant;

import javax.servlet.http.Part;

public class FileHelper {
	private String folder = System.getProperty("jboss.home.dir")+"/gws-produkt-bilder";
	
	public String ladeHoch(Part file, String fileName) {
		if(fileName == null) {
			fileName = "pimg"+Instant.now().getEpochSecond()+"."+welcheExtension(file.getSubmittedFileName());
		}
		
		try (InputStream input = file.getInputStream()) {
	        Files.copy(input, new File(folder, fileName).toPath(), StandardCopyOption.REPLACE_EXISTING);
	        return fileName;
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	public void loesche(String fileName) {
		File f = new File(folder+"/"+fileName);
		f.delete();
	}
	
	public String welcheExtension(String fileName) {
		String ext = "";
		int i = fileName.lastIndexOf('.');
		if (i > 0) {
		    ext = fileName.substring(i+1);
		}
		
		return ext;
	}
}
