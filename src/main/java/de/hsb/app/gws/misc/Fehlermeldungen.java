package de.hsb.app.gws.misc;

public enum Fehlermeldungen {

    KUNDE_LOGIN_EMAIL_EXISTIERT_NICHT("Es existiert kein Benutzer mit dieser E-Mail!"),
    KUNDE_LOGIN_FALSCHES_PASSWORT("Das angegebene Passwort ist falsch!"),
    ADMIN_LOGIN_EMAIL_EXISTIERT_NICHT("Es existiert kein Benutzer mit dieser E-Mail!"),
    ADMIN_LOGIN_FALSCHES_PASSWORT("Das angegebene Passwort ist falsch!"),
	KUNDE_REGISTRIERUNG_EMAIL_WIRD_SCHON_BENUTZT("Die angegebene E-Mail wird bereits von einem anderen Kunden verwendet!"),
	ADMIN_HERSTELLER_LOESCHEN_FEHLER("Der Hersteller konnte nicht gel�scht werden. Stellen Sie sicher, dass der Hersteller mit keinem Produkt verbunden ist!"),
	ADMIN_ADMIN_LOESCHEN_NICHT_MOEGLICH_DA_AKTUELLER_BENUTZER_IN_SESSION("Du kannst diesen Benutzer nicht l�schen, da du mit diesem zurzeit angemeldet bist!"),
	ADMIN_ADMIN_EMAIL_WIRD_BEREITS_VON_EINEM_ANDEREN_ADMIN_BENUTZT("Die angegebene E-Mail wird bereits von einem anderen Admin genutzt!"),
	FORM_ACTION_AUSFUEHREN_FEHLGESCHLAGEN_PLEASE_RELOAD_AND_RETRY("Bei der Ausf�hrung der Aktion ist ein Fehler aufgetreten. Bitte laden Sie die Seite neu und probieren Sie es erneut!"),
	ADMIN_PRODUKT_LOESCHEN_NICHT_MOEGLICH_DA_IN_BESTELLUNG_VERWENDET("Das Produkt kann nicht gel�scht werden, da es in mindestens einer Bestellung verwendet wird. Es wird empfohlen das Produkt zu sperren anstatt das Produkt zu l�schen, um mit der Referenz weiterhin arbeiten zu k�nnen. "),
	PRODUKT_BILD_UPLOAD_FEHLER("Das Bild konnte nicht hochgeladen werden. Bitte �berpr�fen Sie die Berechtigungen des Dateisystems"),
	PRODUKT_BILD_UPLOAD_INVALID_EXT("Ung�ltige Dateiendung. Erlaubt sind: png,jpeg,jpg")
	;

    private final String meldung;

    Fehlermeldungen(final String meldung) {
        this.meldung = meldung;
    }

    @Override
    public String toString() {
        return meldung;
    }
}
