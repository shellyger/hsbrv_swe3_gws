package de.hsb.app.gws.misc;

public enum KundeColumnsFuerOrder {

    ID("id"),
    EMAIL("email"),
    VORNAME("vorname"),
    NACHNAME("nachname")
	;

    private final String column;

    KundeColumnsFuerOrder(final String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column;
    }
}
