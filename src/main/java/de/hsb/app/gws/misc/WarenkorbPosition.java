package de.hsb.app.gws.misc;

public class WarenkorbPosition {
	
	private Long produktId;
	private int anzahl;

	public WarenkorbPosition(Long produktId, int anzahl) {
		this.produktId = produktId;
		this.anzahl = anzahl;
	}

	public Long getProduktId() {
		return produktId;
	}

	public void setProduktId(Long produktId) {
		this.produktId = produktId;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
}
