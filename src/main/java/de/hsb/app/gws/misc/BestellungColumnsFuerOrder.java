package de.hsb.app.gws.misc;

public enum BestellungColumnsFuerOrder {

    ID("id"),
    LIEFER_ADRESSE_NAME("liefer_adresse_name"),
    RECHNUNGS_ADRESSE_NAME("rechnungs_adresse_name"),
    ERSTELLT_ZEIT("erstellt_zeit"),
    STATUS("status")
	;

    private final String column;

    BestellungColumnsFuerOrder(final String column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return column;
    }
}
