package de.hsb.app.gws.dienste;

import javax.ejb.Stateless;

import de.hsb.app.gws.misc.AdminColumnsFuerOrder;
import de.hsb.app.gws.misc.BestellungColumnsFuerOrder;
import de.hsb.app.gws.misc.HerstellerColumnsFuerOrder;
import de.hsb.app.gws.misc.KundeColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ProduktColumnsFuerOrder;

@Stateless
public class OrderByDienst {
	
	public BestellungColumnsFuerOrder findeBestellungColumnFuerOrderEnumByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return BestellungColumnsFuerOrder.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	public KundeColumnsFuerOrder findeKundeColumnFuerOrderEnumByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return KundeColumnsFuerOrder.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}

	public HerstellerColumnsFuerOrder findeHerstellerColumnFuerOrderEnumByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return HerstellerColumnsFuerOrder.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	public ProduktColumnsFuerOrder findeProdukteColumnFuerOrderEnumByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return ProduktColumnsFuerOrder.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	public AdminColumnsFuerOrder findeAdminColumnFuerOrderEnumByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return AdminColumnsFuerOrder.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	public OrderTypes findeOrderTypeByString(String input) {
		if(input == null) {
			return null;
		}
		try {
			return OrderTypes.valueOf(input);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	public OrderTypes getOppositeOrderByType(OrderTypes ot) {
		if(ot == null) {
			return OrderTypes.DESC;
		}
		
		if(ot.equals(OrderTypes.ASC)) {
			return OrderTypes.DESC;
		} else {
			return OrderTypes.ASC;
		}
	}
	
}
