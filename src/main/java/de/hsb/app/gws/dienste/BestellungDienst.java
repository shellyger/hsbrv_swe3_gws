package de.hsb.app.gws.dienste;

import java.util.List;
import javax.ejb.Stateless;
import de.hsb.app.gws.model.Bestellposition;
import de.hsb.app.gws.model.Bestellung;

@Stateless
public class BestellungDienst {
	
	public double getPreisTotal(Bestellung bestellung) {
		double summe = 0;
		
		List<Bestellposition> bestellpositionen = bestellung.getBestellpositionen();
		
		for(Bestellposition bestellposition : bestellpositionen) {
			summe += bestellposition.getProdukt_preis()*bestellposition.getAnzahl();
		}
		
		return summe;
	}
	
}
