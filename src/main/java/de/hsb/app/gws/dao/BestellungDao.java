package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.misc.BestellungColumnsFuerOrder;
import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.model.Bestellung;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class BestellungDao {

	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	public Bestellung findById(Long id) {		
		return em.find(Bestellung.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Bestellung> getAll() {
		return em.createNamedQuery("SelectBestellungen").getResultList();
	}
	
	public boolean save(Bestellung bestellung) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		em.merge(bestellung);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(Bestellung bestellung) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Bestellung b = em.find(Bestellung.class, bestellung.getId());
		em.remove(b);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean create(Bestellung bestellung) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		em.persist(bestellung);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Bestellung> getAllWithOrder(BestellungColumnsFuerOrder orderColumn, OrderTypes orderType) {
		String sColumn = CommonHelper.upperFirst(orderColumn.toString());
		String sType = CommonHelper.upperFirst(orderType.toString());
		
		return em.createNamedQuery("SelectBestellungenOrderBy"+sColumn+sType).getResultList();
	}
}
