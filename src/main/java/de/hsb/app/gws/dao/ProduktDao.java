package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ProduktColumnsFuerOrder;
import de.hsb.app.gws.model.Produkt;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ProduktDao {

	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	
	public Produkt findById(Long id) {		
		return em.find(Produkt.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Produkt> findByGesperrt(boolean gesperrt) {
		Query q = em.createNamedQuery("SelectProdukteWhereGesperrt");
		q.setParameter("gesperrt", gesperrt);
		
		return q.getResultList();
	}
	
	public List<Produkt> getAllGesperrt() {
		return this.findByGesperrt(true);
	}
	
	public List<Produkt> getAllNichtGesperrt() {
		return this.findByGesperrt(false);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Produkt> getAll() {
		return em.createNamedQuery("SelectProdukte").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Produkt> getAllWithOrder(ProduktColumnsFuerOrder orderColumn, OrderTypes orderType) {
		String sColumn = CommonHelper.upperFirst(orderColumn.toString());
		String sType = CommonHelper.upperFirst(orderType.toString());
		
		return em.createNamedQuery("SelectProdukteOrderBy"+sColumn+sType).getResultList();
	}
	
	public boolean save(Produkt produkt) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
				
		em.merge(produkt);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean delete(Produkt produkt) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Produkt p = em.find(Produkt.class, produkt.getId());
		em.remove(p);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean create(Produkt produkt) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		em.persist(produkt);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Produkt> findByGesperrtAndNamedQuery(boolean gesperrt, String namedQuery) {
		Query q = em.createNamedQuery(namedQuery);
		q.setParameter("gesperrt", gesperrt);
		
		return q.getResultList();
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByNameAsc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByNameAsc");
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByNameDesc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByNameDesc");
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByPreisAsc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByPreisAsc");
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByPreisDesc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByPreisDesc");
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByHerstellerNameAsc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByHerstellerNameAsc");
	}
	
	public List<Produkt> getAllNichtGesperrtOrderByHerstellerNameDesc() {
		return this.findByGesperrtAndNamedQuery(false, "SelectProdukteWhereGesperrtOrderByHerstellerNameDesc");
	}
}
