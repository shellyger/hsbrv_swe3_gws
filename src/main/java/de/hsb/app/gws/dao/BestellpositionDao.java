package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.model.Bestellposition;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class BestellpositionDao {
	
	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	public Bestellposition findById(Long id) {		
		return em.find(Bestellposition.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Bestellposition> findByProduktId(Long id) {
		Query q = em.createNamedQuery("SelectBestellpositionenByProduktId");
		q.setParameter("produkt_id", id);
		
		return q.getResultList();
	}
	
	public boolean save(Bestellposition bestellposition) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		em.merge(bestellposition);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(Bestellposition bestellposition) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Bestellposition b = em.find(Bestellposition.class, bestellposition.getId());
		em.remove(b);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean create(Bestellposition bestellposition) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		em.persist(bestellposition);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
