package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.misc.AdminColumnsFuerOrder;
import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.model.Admin;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class AdminDao {
	
	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	public Admin findById(Long id) {		
		return em.find(Admin.class, id);
	}
	
	public Admin findByEmail(String email) {
		Query q = em.createNamedQuery("SelectAdminByEmail");
		q.setParameter("email", email);
		
		try {
			Admin a = (Admin) q.getSingleResult();
			return a;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Admin> getAll() {
		return em.createNamedQuery("SelectAdmins").getResultList();
	}
	
	public boolean save(Admin admin) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		em.merge(admin);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(Admin admin) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Admin a = em.find(Admin.class, admin.getId());
		em.remove(a);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean create(Admin admin) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		em.persist(admin);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Admin> getAllWithOrder(AdminColumnsFuerOrder orderColumn, OrderTypes orderType) {
		String sColumn = CommonHelper.upperFirst(orderColumn.toString());
		String sType = CommonHelper.upperFirst(orderType.toString());
		
		return em.createNamedQuery("SelectAdminsOrderBy"+sColumn+sType).getResultList();
	}

}
