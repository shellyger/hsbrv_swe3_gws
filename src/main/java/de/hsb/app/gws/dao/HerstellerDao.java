package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.HerstellerColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.model.Hersteller;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class HerstellerDao {
	
	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	public Hersteller findById(Long id) {		
		return em.find(Hersteller.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Hersteller> getAll() {
		return em.createNamedQuery("SelectHersteller").getResultList();
	}
	
	public void save(Hersteller hersteller) {
		try {
			utx.begin();
		} catch (NotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		em.merge(hersteller);
		
		try {
			utx.commit();
			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicMixedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicRollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean delete(Hersteller hersteller) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Hersteller h = em.find(Hersteller.class, hersteller.getId());
		em.remove(h);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Hersteller> getAllWithOrder(HerstellerColumnsFuerOrder orderColumn, OrderTypes orderType) {
		String sColumn = CommonHelper.upperFirst(orderColumn.toString());
		String sType = CommonHelper.upperFirst(orderType.toString());
		
		return em.createNamedQuery("SelectHerstellerOrderBy"+sColumn+sType).getResultList();
	}
	
	public boolean create(Hersteller hersteller) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		em.persist(hersteller);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	
}
