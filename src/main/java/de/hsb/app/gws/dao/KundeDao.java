package de.hsb.app.gws.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.KundeColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.model.Kunde;

@Stateless
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class KundeDao {
	
	@PersistenceContext(name="gws-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	public Kunde findById(Long id) {		
		return em.find(Kunde.class, id);
	}
	
	public Kunde findByEmail(String email) {
		Query q = em.createNamedQuery("SelectKundeByEmail");
		q.setParameter("email", email);
		
		try {
			Kunde k = (Kunde) q.getSingleResult();
			return k;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Kunde> getAll() {
		return em.createNamedQuery("SelectKunden").getResultList();
	}
	
	public boolean save(Kunde kunde) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		em.merge(kunde);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean delete(Kunde kunde) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		Kunde a = em.find(Kunde.class, kunde.getId());
		em.remove(a);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean create(Kunde kunde) {
		try {
			utx.begin();
		} catch (NotSupportedException | SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		em.persist(kunde);
		
		try {
			utx.commit();
			return true;
		} catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Kunde> getAllWithOrder(KundeColumnsFuerOrder orderColumn, OrderTypes orderType) {
		String sColumn = CommonHelper.upperFirst(orderColumn.toString());
		String sType = CommonHelper.upperFirst(orderType.toString());
		
		return em.createNamedQuery("SelectKundenOrderBy"+sColumn+sType).getResultList();
	}

}
