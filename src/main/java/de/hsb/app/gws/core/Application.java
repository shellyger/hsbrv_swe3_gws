package de.hsb.app.gws.core;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.h2.store.fs.FileUtils;
import org.h2.util.IOUtils;

import de.hsb.app.gws.dao.AdminDao;
import de.hsb.app.gws.dao.BestellpositionDao;
import de.hsb.app.gws.dao.BestellungDao;
import de.hsb.app.gws.dao.HerstellerDao;
import de.hsb.app.gws.dao.KundeDao;
import de.hsb.app.gws.dao.ProduktDao;
import de.hsb.app.gws.misc.BestellungStatus;
import de.hsb.app.gws.model.Admin;
import de.hsb.app.gws.model.Bestellposition;
import de.hsb.app.gws.model.Bestellung;
import de.hsb.app.gws.model.Hersteller;
import de.hsb.app.gws.model.Kunde;
import de.hsb.app.gws.model.Produkt;

@Singleton
@Startup
public class Application {
	
	@Inject
	private ProduktDao produktDao;
	
	@Inject
	private HerstellerDao herstellerDao;
	
	@Inject
	private BestellungDao bestellungDao;
	
	@Inject
	private KundeDao kundeDao;
	
	@Inject
	private BestellpositionDao bestellpositionDao;
	
	@Inject
	private AdminDao adminDao;
	
	@PostConstruct
	public void init() {
		initDirs();
		erstelleDefaultEintraege();
	}
	
	/**
	 * Initialisiere das Upload Verzeichnis
	 * 
	 */
	private void initDirs() {
		String dir = System.getProperty("jboss.home.dir")+"/gws-produkt-bilder";
		
		FileUtils.deleteRecursive(dir, false);
		FileUtils.createDirectory(dir);
		
		try {
			ladeDieBilderRunter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Lade die Produkt Bilder f�r die Initial-Produkte herunter
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private void ladeDieBilderRunter() throws MalformedURLException, IOException {
		List<String> namen = Arrays.asList("pimg_tpl_cocacola.jpg", "pimg_tpl_pepsi.jpg", "pimg_tpl_spezi_cocacola_cherry.jpg", "pimg_tpl_cocacolazero.jpg");
		
		String dir = System.getProperty("jboss.home.dir")+"/gws-produkt-bilder";
		
		for (String name : namen) {
			InputStream inputStream = new URL("https://rflow.blob.core.windows.net/rflow-pub/"+name).openStream();
			FileOutputStream fileOS = new FileOutputStream(dir+"/"+name);
			IOUtils.copy(inputStream, fileOS);			
		}
	}
	
	/**
	 * Erstellt die Intial-Entit�ten
	 * 
	 */
	private void erstelleDefaultEintraege() {
		Hersteller herstellerCocaCola = new Hersteller("Coca Cola");
		Hersteller herstellerPepsiCo = new Hersteller("PepsiCo");
		
		herstellerDao.create(herstellerCocaCola);
		herstellerDao.create(herstellerPepsiCo);

		herstellerDao.create(new Hersteller("Vilsa"));
		
		// @todo: das muss iwie anders. Aber gehts vorerst so
		Produkt produkt1 = new Produkt("Coca Cola Cherry", "Ich bin eine leckere Cola ....", 1.99, false, herstellerCocaCola, "pimg_tpl_spezi_cocacola_cherry.jpg");
		produktDao.create(produkt1);
		Produkt produkt2 = new Produkt("Coca Cola", "Ich bin eine leckere Cola ....", 1.99, false, herstellerCocaCola, "pimg_tpl_cocacola.jpg");
		produktDao.create(produkt2);
		Produkt produkt3 = new Produkt("Coca Cola Zero", "Ich bin eine leckere Cola ....", 2.99, false, herstellerCocaCola, "pimg_tpl_cocacolazero.jpg");
		produktDao.create(produkt3);
		Produkt produkt4 = new Produkt("Pepsi Cola", "Ich bin eine leckere Cola ....", 0.99, false, herstellerPepsiCo, "pimg_tpl_pepsi.jpg");
		produktDao.create(produkt4);
		
		
		Kunde k1 = new Kunde("Michel", "Zudse", "info@mz.de", "123");
		Kunde k2 = new Kunde("Peter", "Lustig", "info@1mz.de", "123");
		Kunde k3 = new Kunde("Peter", "Ralf", "info@6mz.de", "123");
		Kunde k4 = new Kunde("David", "Heinz", "info@3mz.de", "123");
		Kunde k5 = new Kunde("Alex", "R", "info3@mz.de", "123");
		Kunde k6 = new Kunde("Hakan", "B", "info4@mz.de", "123");
		
		kundeDao.create(k1);
		kundeDao.create(k2);
		kundeDao.create(k3);
		kundeDao.create(k4);
		kundeDao.create(k5);
		kundeDao.create(k6);
		
		Bestellung b1 = new Bestellung();
		b1.setKunde(k1);
		b1.setLiefer_adresse_name("Max Mustermann");
		b1.setLiefer_adresse_strasse("Musterstra�e 1");
		b1.setLiefer_adresse_plz("12345");
		b1.setLiefer_adresse_stadt("Musterstadt");
		b1.setRechnungs_adresse_name("Max Mustermann");
		b1.setRechnungs_adresse_strasse("Musterstra�e 1");
		b1.setRechnungs_adresse_plz("12345");
		b1.setRechnungs_adresse_stadt("Musterstadt");
		b1.setStatus(BestellungStatus.OFFEN);
		bestellungDao.create(b1);
	
		Bestellposition bestellposition1 = new Bestellposition(b1, produkt1, produkt1.getId(), produkt1.getName(), produkt1.getPreis(), 1);
		bestellpositionDao.create(bestellposition1);
		
		adminDao.create(new Admin("Admin","Admin","admin@admin.de", "admin"));
		adminDao.create(new Admin("Michel","Zudse","michel@zudse.de", "123"));
	}
	
}
