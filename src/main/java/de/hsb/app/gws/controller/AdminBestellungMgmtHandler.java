package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.faces.annotation.ManagedProperty;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.BestellungDao;
import de.hsb.app.gws.dienste.BestellungDienst;
import de.hsb.app.gws.dienste.OrderByDienst;
import de.hsb.app.gws.misc.BestellungColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Bestellung;

@ManagedBean
@Named("adminBestellungMgmtHandler")
@ViewScoped
public class AdminBestellungMgmtHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
	
	private Bestellung bestellung;
	
	private String orderBy;
	
	private String orderColumn;
	
	private BestellungColumnsFuerOrder orderByColumnS;
	
	private OrderTypes orderByType;
	
	private List<Bestellung> bestellungen;

	@Inject
	OrderByDienst orderByDienst;

	@Inject
	BestellungDao bestellungDao;
	
	@Inject
	BestellungDienst bestellungDienst;

	public void initAdminBestellungMgmtPage() throws IOException {
		if(id == null) {
			ResponseHelper.redirectTo("bestellungen.xhtml");
		}
		
		bestellung = bestellungDao.findById(id);
		
		if(bestellung == null) {
			ResponseHelper.redirectTo("bestellungen.xhtml");
		}
	}
	
	public void initAdminBestellungenListe() {
		if(orderBy == null) {
			orderBy = OrderTypes.DESC.name();
		}
		
		orderByColumnS = orderByDienst.findeBestellungColumnFuerOrderEnumByString(orderColumn);
		orderByType = orderByDienst.findeOrderTypeByString(orderBy);
		
		
		if(orderByColumnS == null || orderByType == null) {
			bestellungen = bestellungDao.getAll();
		} else {
			bestellungen = bestellungDao.getAllWithOrder(orderByColumnS, orderByType);
		}
	}

	public void aendereBestellungStatus() {
		bestellungDao.save(bestellung);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bestellung getBestellung() {
		return bestellung;
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}

	public List<Bestellung> getBestellungen() {
		return bestellungen;
	}

	public void setBestellungen(List<Bestellung> bestellungen) {
		this.bestellungen = bestellungen;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public BestellungColumnsFuerOrder getOrderByColumnS() {
		return orderByColumnS;
	}

	public OrderTypes getOrderByType() {
		return orderByType;
	}
	
	public OrderByDienst getOrderByDienst() {
		return orderByDienst;
	}
	
	public BestellungDienst getBestellungDienst() {
		return bestellungDienst;
	}
	
}
