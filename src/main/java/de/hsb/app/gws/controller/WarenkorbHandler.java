package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.BestellpositionDao;
import de.hsb.app.gws.dao.BestellungDao;
import de.hsb.app.gws.dao.ProduktDao;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.misc.WarenkorbPosition;
import de.hsb.app.gws.misc.WarenkorbPositionFuerVerarbeitung;
import de.hsb.app.gws.model.Bestellposition;
import de.hsb.app.gws.model.Bestellung;
import de.hsb.app.gws.model.Produkt;

@ManagedBean
@Named("warenkorbHandler")
@SessionScoped
public class WarenkorbHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<WarenkorbPosition> warenkorbPositionen = new ArrayList<>();
	
	private Bestellung bestellung;

	@Inject
	BestellungDao bestellungDao;
	
	@Inject
	ProduktDao produktDao;
	
	@Inject
	BestellpositionDao bestellpositionDao;
	
	@Inject 
	UserSessionHandler userSessionHandler;
	
	HashMap<String, Object> warenkorbposition_verarbeitung;
	
	List<WarenkorbPositionFuerVerarbeitung> warenkorbposition_verarbeitung_only;

	public void initCheckoutPage() throws IOException {
		if(userSessionHandler.getKunde() == null) {
			ResponseHelper.redirectTo("kundenlogin.xhtml");
		}
		
		if(warenkorbPositionen.size() == 0) {
			ResponseHelper.redirectTo("warenkorb.xhtml");
		}
		
		bestellung = new Bestellung();
	}
	
	@SuppressWarnings("unchecked")
	public void initWarenkorbPage() {
		warenkorbposition_verarbeitung = holeAusDatenbankFuerAlleWarenkorbPositionen();
		warenkorbposition_verarbeitung_only = (List<WarenkorbPositionFuerVerarbeitung>) warenkorbposition_verarbeitung.get("warenkorbpositionen");
	}

	public double getPreisTotal() {
		return (double) warenkorbposition_verarbeitung.get("preis_total");
	}
	
	public HashMap<String, Object> getWarenkorbposition_verarbeitung() {
		return warenkorbposition_verarbeitung;
	}

	public void setWarenkorbposition_verarbeitung(HashMap<String, Object> warenkorbposition_verarbeitung) {
		this.warenkorbposition_verarbeitung = warenkorbposition_verarbeitung;
	}
	
	
	public List<WarenkorbPositionFuerVerarbeitung> getWarenkorbposition_verarbeitung_only() {
		return warenkorbposition_verarbeitung_only;
	}

	public void setWarenkorbposition_verarbeitung_only(
		List<WarenkorbPositionFuerVerarbeitung> warenkorbposition_verarbeitung_only) {
		this.warenkorbposition_verarbeitung_only = warenkorbposition_verarbeitung_only;
	}

	public void zumWarenkorbHinzufuegen(Produkt produkt, int anzahl) throws Exception {
		for(int i=0; i < warenkorbPositionen.size(); i++) {
			WarenkorbPosition tmpPosition = warenkorbPositionen.get(i);
			
			if(tmpPosition.getProduktId().equals(produkt.getId())) {			
				int anzahlNeu = tmpPosition.getAnzahl()+anzahl;
				WarenkorbPosition positionNeu = new WarenkorbPosition(produkt.getId(), anzahlNeu);
				
				warenkorbPositionen.set(i, positionNeu);
				return;
			}
		}
		
		warenkorbPositionen.add(new WarenkorbPosition(produkt.getId(), anzahl));
	}
	
	/**
	 * Hole de Objekte der Entitäten der Produkte aus der DB
	 * 
	 * @return HashMap mit den warenkorbpositionen und dem Total Preis
	 */
	public HashMap<String, Object> holeAusDatenbankFuerAlleWarenkorbPositionen() {
		HashMap<String, Object> r = new HashMap<>();
		
		List<WarenkorbPositionFuerVerarbeitung> neuFuerVerarbeitung = new ArrayList<>();
		
		double preis_total = 0;
		
		for(int i=0; i < warenkorbPositionen.size(); i++) {
			
			WarenkorbPosition tmpPosition = warenkorbPositionen.get(i);
			Produkt p = produktDao.findById(tmpPosition.getProduktId());
			if(p == null || p.isGesperrt()) {
				// produkt existiert nicht mehr oder wurde zwischenzeitlich gesperrt
				warenkorbPositionen.remove(tmpPosition);
			} else {
				preis_total +=p.getPreis()*tmpPosition.getAnzahl();
				neuFuerVerarbeitung.add(new WarenkorbPositionFuerVerarbeitung(p, tmpPosition));
			}
		}
		
		r.put("preis_total", preis_total);
		r.put("warenkorbpositionen", neuFuerVerarbeitung);
		
		return r;
	}
	
	public int getAnzahlTotal() {
		int anzahl_total = 0;
		
		for(WarenkorbPosition warenkorbPosition : warenkorbPositionen) {
			anzahl_total += warenkorbPosition.getAnzahl();
		}
		
		return anzahl_total;
	}
	
	public String ausDemWarenkorbEntfernen(WarenkorbPosition warenkorbPosition) {
		warenkorbPositionen.remove(warenkorbPosition);
		
		return "warenkorb?faces-redirect=true";
	}
		
	public String fuehreBestellungAus() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		bestellung.setKunde(userSessionHandler.getKunde());
		
		if(!erstelleBestellung()) {
			facesContext.addMessage("warenkorb-form-checkout:submit-button",  new FacesMessage(Fehlermeldungen.FORM_ACTION_AUSFUEHREN_FEHLGESCHLAGEN_PLEASE_RELOAD_AND_RETRY.toString()));
			return null;
		}
		
		// Warenkorb zurücksetzen
		warenkorbPositionen = new ArrayList<>();
		
		return "bestellung_abgeschlossen?faces-redirect=true";
	}
	
	private boolean erstelleBestellung() {
		initWarenkorbPage();
		
		if(warenkorbposition_verarbeitung_only.size() == 0) {
			return false;
		}
		
		if(!bestellungDao.create(bestellung)) {
			return false;
		}
		
		warenkorbposition_verarbeitung_only.forEach(
			pos -> bestellpositionDao.create(
					new Bestellposition(
							bestellung,
							pos.getProdukt(),
							pos.getProdukt().getId(),
							pos.getProdukt().getName(),
							pos.getProdukt().getPreis(),
							pos.getWarenkorbPosition().getAnzahl()
							)
					)
		);
		
		return true;
	}
	
	public String geheZumCheckout() {		
		if(userSessionHandler.getKunde() == null) {
			return "kundenlogin?onFinishGoTo=checkout&faces-redirect=true";
		} else {
			return "checkout?faces-redirect=true";
		}
	}
	
	public List<WarenkorbPosition> getWarenkorbPositionen() {
		return warenkorbPositionen;
	}

	public void setWarenkorbPositionen(List<WarenkorbPosition> warenkorbPositionen) {
		this.warenkorbPositionen = warenkorbPositionen;
	}

	public Bestellung getBestellung() {
		return bestellung;
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}
	
}
