package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

import de.hsb.app.gws.dao.BestellpositionDao;
import de.hsb.app.gws.dao.HerstellerDao;
import de.hsb.app.gws.dao.ProduktDao;
import de.hsb.app.gws.dienste.OrderByDienst;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.FileHelper;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ProduktColumnsFuerOrder;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Hersteller;
import de.hsb.app.gws.model.Produkt;

@ManagedBean
@Named("adminProduktMgmtHandler")
@ViewScoped
public class AdminProduktMgmtHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
	
	private Produkt produkt;
	
	private Produkt produktNeu;

	private List<Produkt> produkteListe;
	
	private List<Hersteller> hersteller;
	
	private Part imageFile;
	
	private String orderBy;
	
	private String orderColumn;
	
	private ProduktColumnsFuerOrder orderByColumnS;
	
	private OrderTypes orderByType;
	
	private FileHelper fileHelper = new FileHelper();
	
	@Inject
	OrderByDienst orderByDienst;
	
	@Inject
	ProduktDao produktDao;
	
	@Inject
	BestellpositionDao bestellpositionDao;
	
	@Inject
	HerstellerDao herstellerDao;
	
	public void initAdminProduktMgmtPage() throws IOException {
		if(id == null) {
			ResponseHelper.redirectTo("produkte.xhtml");
		}
		
		produkt = produktDao.findById(id);
		
		if(produkt == null) {
			ResponseHelper.redirectTo("produkte.xhtml");
		}
		
		setHersteller(herstellerDao.getAll());
	}
	
	public void initAdminProduktNeuPage() {
		produktNeu = new Produkt();
		
		setHersteller(herstellerDao.getAll());
	
		if(hersteller.size() > 0) {
			produktNeu.setHersteller(hersteller.get(0));
		}
	}
	
	public void initAdminProdukteListe() {
		if(orderBy == null) {
			orderBy = OrderTypes.DESC.name();
		}
		
		orderByColumnS = orderByDienst.findeProdukteColumnFuerOrderEnumByString(orderColumn);
		orderByType = orderByDienst.findeOrderTypeByString(orderBy);
		
		
		if(orderByColumnS == null || orderByType == null) {
			produkteListe = produktDao.getAll();
		} else {
			produkteListe = produktDao.getAllWithOrder(orderByColumnS, orderByType);
		}
	}
	
	public String erstelleProdukt() {		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		if(!isValidExt()) {
			facesContext.addMessage("produkt-form-neu:bild",  new FacesMessage(Fehlermeldungen.PRODUKT_BILD_UPLOAD_INVALID_EXT.toString()));
			return null;
		}
		
		String fileNameFuerDb = fileHelper.ladeHoch(imageFile, null);
		
		if(fileNameFuerDb == null) {
			facesContext.addMessage("produkt-form-neu:bild",  new FacesMessage(Fehlermeldungen.PRODUKT_BILD_UPLOAD_FEHLER.toString()));
			return null;
		}
		
		produktNeu.setBild(fileNameFuerDb);
		
		if(!produktDao.create(produktNeu)) {
			fileHelper.loesche(fileNameFuerDb);
			facesContext.addMessage("produkt-form-neu:submit-button",  new FacesMessage(Fehlermeldungen.FORM_ACTION_AUSFUEHREN_FEHLGESCHLAGEN_PLEASE_RELOAD_AND_RETRY.toString()));
			return null;
		}
				
		return "produkte?faces-redirect=true";
	}
	
	public String speichereAenderungen() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		if(imageFile != null) {
			if(!isValidExt()) {
				facesContext.addMessage("produkt-form-mgmt-edit:bild",  new FacesMessage(Fehlermeldungen.PRODUKT_BILD_UPLOAD_INVALID_EXT.toString()));
				return null;
			}
			
			if(fileHelper.ladeHoch(imageFile, produkt.getBild()) == null) {
				facesContext.addMessage("produkt-form-mgmt-edit:bild",  new FacesMessage(Fehlermeldungen.PRODUKT_BILD_UPLOAD_FEHLER.toString()));
				return null;
			}
		}
		
		if(!produktDao.save(produkt)) {
			facesContext.addMessage("produkt-form-mgmt-edit:submit-button",  new FacesMessage(Fehlermeldungen.FORM_ACTION_AUSFUEHREN_FEHLGESCHLAGEN_PLEASE_RELOAD_AND_RETRY.toString()));
			return null;
		}
		return "/admin/produkte?faces-redirect=true";
	}
	
	private boolean isValidExt() {
		String ext = fileHelper.welcheExtension(imageFile.getSubmittedFileName());
		List<String> validExt = Arrays.asList("png", "jpeg", "jpg");
		if(ext == null || "".equals(ext) || !validExt.contains(ext)) {
			return false;
		}
		
		return true;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public List<Hersteller> getHersteller() {
		return hersteller;
	}

	public void setHersteller(List<Hersteller> hersteller) {
		this.hersteller = hersteller;
	}

	public Produkt getProduktNeu() {
		return produktNeu;
	}

	public void setProduktNeu(Produkt produktNeu) {
		this.produktNeu = produktNeu;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public ProduktColumnsFuerOrder getOrderByColumnS() {
		return orderByColumnS;
	}

	public OrderTypes getOrderByType() {
		return orderByType;
	}

	public List<Produkt> getProdukteListe() {
		return produkteListe;
	}

	public void setProdukteListe(List<Produkt> produkteListe) {
		this.produkteListe = produkteListe;
	}
	
	public OrderByDienst getOrderByDienst() {
		return orderByDienst;
	}

	public Part getImageFile() {
		return imageFile;
	}

	public void setImageFile(Part imageFile) {
		this.imageFile = imageFile;
	}

}
