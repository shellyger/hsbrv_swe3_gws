package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.annotation.ManagedProperty;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.hsb.app.gws.dao.HerstellerDao;
import de.hsb.app.gws.dao.ProduktDao;
import de.hsb.app.gws.misc.CommonHelper;
import de.hsb.app.gws.misc.ProduktHelper;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Produkt;

@ManagedBean
@Named("produktDetailsHandler")
@SessionScoped
public class ProduktDetailsHandler implements Serializable {

	public static Logger logger = Logger.getLogger(ProduktDetailsHandler.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
	private Produkt produkt;
	
	private int anzahlWarenkorb = 1;
	
	private List<Integer> anzahl_select_auswahl_warenkorb = ProduktHelper.getPossibleSelectQuantitie();
	
	private List<Produkt> weitereProdukte;
	
	@Inject
	ProduktDao produktDao;
	
	@Inject
	HerstellerDao herstellerDao;

	public void loadProduktDetails() throws IOException {
		// Wurde die ID gesetzt? Id kann auch null sein, wenn keine long als param angegeben wurde
		if(id == null) {
			ResponseHelper.redirectTo("index.xhtml");
		}
		
		// zur�cksetzen auf null
		// Grund: @SessionScoped, mit @RequestScoped m�sste man das nicht. Aber sonst ist das Objekt null f�r den Warenkorb
		produkt = null;
		
		// zur�cksetzen auf 1
		// Grund: @SessionScoped, mit @RequestScoped m�sste man das nicht.
		anzahlWarenkorb = 1;
		
		weitereProdukte = new ArrayList<>();
		
		produkt = produktDao.findById(id);
		
		// id nicht vorhanden in db oder gesperrt
		if(produkt == null || produkt.isGesperrt()) {
			ResponseHelper.redirectTo("index.xhtml");
		}
		
		anzahl_select_auswahl_warenkorb = ProduktHelper.getPossibleSelectQuantitie();
		
		weitereProdukte = CommonHelper.nurDieErstenNObjekteOhneDasAngegebeneObjekt(produkt.getHersteller().getProdukte(), produkt, 4);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	
	public int getAnzahlWarenkorb() {
		return anzahlWarenkorb;
	}

	public void setAnzahlWarenkorb(int anzahlWarenkorb) {
		this.anzahlWarenkorb = anzahlWarenkorb;
	}

	public List<Integer> getAnzahl_select_auswahl_warenkorb() {
		return anzahl_select_auswahl_warenkorb;
	}

	public void setAnzahl_select_auswahl_warenkorb(List<Integer> anzahl_select_auswahl_warenkorb) {
		this.anzahl_select_auswahl_warenkorb = anzahl_select_auswahl_warenkorb;
	}

	public List<Produkt> getWeitereProdukte() {
		return weitereProdukte;
	}

	public void setWeitereProdukte(List<Produkt> weitereProdukte) {
		this.weitereProdukte = weitereProdukte;
	}

}
