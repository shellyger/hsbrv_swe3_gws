package de.hsb.app.gws.controller;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import de.hsb.app.gws.misc.BestellungStatus;
import de.hsb.app.gws.misc.IndexProduktSortierAuswahl;


@ManagedBean
@Named("commonHandler")
@ApplicationScoped
public class Common {
	public BestellungStatus[] getBestellungStatuses() {
		return BestellungStatus.values();
	}
	
	public IndexProduktSortierAuswahl[] getProdukteSortierAuswahlmoeglichkeiten() {
		return IndexProduktSortierAuswahl.values();
	}
}
