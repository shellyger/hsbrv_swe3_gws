package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.KundeDao;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Kunde;

@ManagedBean()
@Named("kundenLoginHandler")
@RequestScoped
public class KundenLoginHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String credEmail;
	private String credPasswort;
	
	private String registerEmail;
	private String registerPasswort;
	private String registerVorname;
	private String registerNachname;
	
	private String onFinishGoTo;
	
	@Inject
	KundeDao kundeDao;
	
	@Inject 
	UserSessionHandler userSessionHandler;

	public void initLoginPage() throws IOException {
		if(userSessionHandler.getKunde() != null) {
			ResponseHelper.redirectTo("index.xhtml");
		}
	}
	
	public boolean istOnFishGoToCheckoutGesetzt() {
		if(onFinishGoTo == null) {
			return false;
		}
		
		return onFinishGoTo.equals("checkout");
	}
	
	public boolean istAlsAdminAngemeldet() {
		if(userSessionHandler.getAdmin() == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean istAlsKundeAngemeldet() {
		if(userSessionHandler.getKunde() == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public String anmeldungDurchfuehren() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		Kunde kundeAusDb = kundeDao.findByEmail(credEmail);
		
		// @todo: das hier evtl als dienst auslagern
		if(kundeAusDb == null) {
			facesContext.addMessage("kunden-form-login:email",  new FacesMessage(Fehlermeldungen.KUNDE_LOGIN_EMAIL_EXISTIERT_NICHT.toString()));
			return null;
		} else {
			if(!kundeAusDb.getPasswort().equals(credPasswort)) {
				/* 
				 * Eigentlich sollte man aus Sicherheitsgr�nden nicht die folgende Nachricht ausgeben.
				 * Um hier im Projekt die Passwort �berpr�fung darzustellen, geben wir diese Nachricht aus.
				*/
				facesContext.addMessage("kunden-form-login:passwort",  new FacesMessage(Fehlermeldungen.KUNDE_LOGIN_FALSCHES_PASSWORT.toString()));
				return null;
			} else {
				userSessionHandler.setKunde(kundeAusDb);
				if(istOnFishGoToCheckoutGesetzt()) {
					return "checkout?faces-redirect=true";
				}
				return "index?faces-redirect=true";
			}
		}
	}
	
	public String registrierungDurchfuehren() {
		Kunde k = new Kunde(registerVorname,registerNachname,registerEmail,registerPasswort);
				
		if(!kundeDao.create(k)) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("kunden-form-register:register_email",  new FacesMessage(Fehlermeldungen.KUNDE_REGISTRIERUNG_EMAIL_WIRD_SCHON_BENUTZT.toString()));
			return null;
		} else {
			userSessionHandler.setKunde(k);
			if(istOnFishGoToCheckoutGesetzt()) {
				return "checkout?faces-redirect=true";
			}
			return "index?faces-redirect=true";
		}
	}
	
	public String logout() {
		userSessionHandler.setKunde(null);
		return "kundenlogin?faces-redirect=true";
	}

	public String getCredPasswort() {
		return credPasswort;
	}

	public void setCredPasswort(String credPasswort) {
		this.credPasswort = credPasswort;
	}

	public String getCredEmail() {
		return credEmail;
	}

	public void setCredEmail(String credEmail) {
		this.credEmail = credEmail;
	}
	
	public String getRegisterEmail() {
		return registerEmail;
	}

	public void setRegisterEmail(String registerEmail) {
		this.registerEmail = registerEmail;
	}

	public String getRegisterPasswort() {
		return registerPasswort;
	}

	public void setRegisterPasswort(String registerPasswort) {
		this.registerPasswort = registerPasswort;
	}

	public String getRegisterVorname() {
		return registerVorname;
	}

	public void setRegisterVorname(String registerVorname) {
		this.registerVorname = registerVorname;
	}

	public String getRegisterNachname() {
		return registerNachname;
	}

	public void setRegisterNachname(String registerNachname) {
		this.registerNachname = registerNachname;
	}

	public String getOnFinishGoTo() {
		return onFinishGoTo;
	}

	public void setOnFinishGoTo(String onFinishGoTo) {
		this.onFinishGoTo = onFinishGoTo;
	}

}
