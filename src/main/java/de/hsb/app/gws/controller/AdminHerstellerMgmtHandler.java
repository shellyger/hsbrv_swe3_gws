package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.HerstellerDao;
import de.hsb.app.gws.dienste.OrderByDienst;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.HerstellerColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Hersteller;

@ManagedBean
@Named("adminHerstellerMgmtHandler")
@ViewScoped
public class AdminHerstellerMgmtHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
		
	private Hersteller hersteller;
	
	private List<Hersteller> herstellerListe;
	
	@Inject
	HerstellerDao herstellerDao;
	
	private String orderBy;
	
	private String orderColumn;
	
	@Inject
	OrderByDienst orderByDienst;
	
	private Hersteller herstellerNeu;

	private HerstellerColumnsFuerOrder orderByColumnS;
	private OrderTypes orderByType;


	public void initAdminHerstellerMgmtPage() throws IOException {
		if(id == null) {
			ResponseHelper.redirectTo("hersteller.xhtml");
		}
		
		hersteller = herstellerDao.findById(id);
		
		if(hersteller == null) {
			ResponseHelper.redirectTo("hersteller.xhtml");
		}
	}
	
	public void initAdminHerstellerNeuPage() {
		herstellerNeu = new Hersteller();
	}
	
	public void initAdminHerstellerListe() {
		if(orderBy == null) {
			orderBy = OrderTypes.DESC.name();
		}
		
		orderByColumnS = orderByDienst.findeHerstellerColumnFuerOrderEnumByString(orderColumn);
		orderByType = orderByDienst.findeOrderTypeByString(orderBy);
		
		if(orderByColumnS == null || orderByType == null) {
			herstellerListe = herstellerDao.getAll();
		} else {
			herstellerListe = herstellerDao.getAllWithOrder(orderByColumnS, orderByType);
		}
	}
	
	public String erstelleHersteller() {
		herstellerDao.create(herstellerNeu);
		return "hersteller?faces-redirect=true";
	}
	
	public String speichereAenderungen() {
		herstellerDao.save(hersteller);
		return "hersteller?faces-redirect=true";
	}
	
	public String loeschen() {		
		if(!herstellerDao.delete(hersteller)) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("hersteller-form-mgmt-edit:delete-button",  new FacesMessage(Fehlermeldungen.ADMIN_HERSTELLER_LOESCHEN_FEHLER.toString()));
			return null;	
		}
		
		
		return "hersteller?faces-redirect=true";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Hersteller getHersteller() {
		return hersteller;
	}

	public void setHersteller(Hersteller hersteller) {
		this.hersteller = hersteller;
	}

	public List<Hersteller> getHerstellerListe() {
		return herstellerListe;
	}

	public void setHerstellerListe(List<Hersteller> herstellerListe) {
		this.herstellerListe = herstellerListe;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public HerstellerColumnsFuerOrder getOrderByColumnS() {
		return orderByColumnS;
	}

	public OrderTypes getOrderByType() {
		return orderByType;
	}
	
	public OrderByDienst getOrderByDienst() {
		return orderByDienst;
	}

	public Hersteller getHerstellerNeu() {
		return herstellerNeu;
	}

	public void setHerstellerNeu(Hersteller herstellerNeu) {
		this.herstellerNeu = herstellerNeu;
	}
	
}
