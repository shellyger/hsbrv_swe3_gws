package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.AdminDao;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Admin;

@ManagedBean()
@Named("adminLoginHandler")
@RequestScoped
public class AdminLoginHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String credEmail;
	private String credPasswort;
	
	@Inject
	AdminDao adminDao;
	
	@Inject 
	UserSessionHandler userSessionHandler;

	public void initLoginPage() throws IOException {
		if(userSessionHandler.getAdmin() != null) {
			ResponseHelper.redirectTo("admin/index.xhtml");
		}
	}
	
	public String anmeldungDurchfuehren() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		Admin adminAusDb = adminDao.findByEmail(credEmail);
		
		if(adminAusDb == null) {
			facesContext.addMessage("admin-form-login:email",  new FacesMessage(Fehlermeldungen.ADMIN_LOGIN_EMAIL_EXISTIERT_NICHT.toString()));
			return null;
		} else {
			if(!adminAusDb.getPasswort().equals(credPasswort)) {
				/* 
				 * Eigentlich sollte man aus Sicherheitsgr�nden nicht die folgende Nachricht ausgeben.
				 * Um hier im Projekt die Passwort �berpr�fung darzustellen, geben wir diese Nachricht aus.
				*/
				facesContext.addMessage("admin-form-login:passwort",  new FacesMessage(Fehlermeldungen.ADMIN_LOGIN_FALSCHES_PASSWORT.toString()));
				return null;
			} else {
				userSessionHandler.setAdmin(adminAusDb);
				return "admin/index?faces-redirect=true";
			}
		}
		
	}
	
	public String logout() {
		userSessionHandler.setAdmin(null);
		return "/adminlogin?faces-redirect=true";
	}
	
	public void pruefeAdminBerechtigung() throws IOException {		
		if(userSessionHandler.getAdmin() == null) {
			ResponseHelper.redirectTo("../adminlogin.xhtml");
		}
	}

	public String getCredPasswort() {
		return credPasswort;
	}

	public void setCredPasswort(String credPasswort) {
		this.credPasswort = credPasswort;
	}

	public String getCredEmail() {
		return credEmail;
	}

	public void setCredEmail(String credEmail) {
		this.credEmail = credEmail;
	}

}
