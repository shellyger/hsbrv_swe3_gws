package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.AdminDao;
import de.hsb.app.gws.dienste.OrderByDienst;
import de.hsb.app.gws.misc.AdminColumnsFuerOrder;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Admin;

@ManagedBean
@Named("adminAdminMgmtHandler")
@ViewScoped
public class AdminAdminMgmtHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
		
	private Admin admin;
	
	private List<Admin> admins;
	
	private Admin adminNeu;
	
	private String orderBy;
	
	private String orderColumn;
	
	private AdminColumnsFuerOrder orderByColumnS;
	
	private OrderTypes orderByType;

	@Inject
	OrderByDienst orderByDienst;

	@Inject
	AdminDao adminDao;
	
	@Inject
	UserSessionHandler userSessionHandler;

	@PostConstruct
	public void init() {
		/*userSessionHandler.setAdmin(new Admin());
		userSessionHandler.setKunde(new Kunde());*/
	}
	
	public void initAdminAdminMgmtPage() throws IOException {
		if(id == null) {
			ResponseHelper.redirectTo("admins.xhtml");
		}
		
		admin = adminDao.findById(id);
		
		if(admin == null) {
			ResponseHelper.redirectTo("admins.xhtml");
		}
	}
	
	public void initAdminAdminListe() {
		if(orderBy == null) {
			orderBy = OrderTypes.DESC.name();
		}
		
		orderByColumnS = orderByDienst.findeAdminColumnFuerOrderEnumByString(orderColumn);
		orderByType = orderByDienst.findeOrderTypeByString(orderBy);
		
		
		if(orderByColumnS == null || orderByType == null) {
			admins = adminDao.getAll();
		} else {
			admins = adminDao.getAllWithOrder(orderByColumnS, orderByType);
		}
	}
	
	public void initAdminAdminNeuPage() {
		adminNeu = new Admin();
	}
	
	public String erstelleAdmin() {
		if(adminDao.findByEmail(adminNeu.getEmail()) != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("admin-form-neu:submit-button",  new FacesMessage(Fehlermeldungen.ADMIN_ADMIN_EMAIL_WIRD_BEREITS_VON_EINEM_ANDEREN_ADMIN_BENUTZT.toString()));
			return null;
		}
		
		adminDao.create(adminNeu);
		
		return "admins?faces-redirect=true";
	}
	
	public String speichereAenderungen() {
		Admin adminEmailSuche = adminDao.findByEmail(admin.getEmail());
		
		if(adminEmailSuche != null) {
			if(!adminEmailSuche.getId().equals(admin.getId())) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.addMessage("admin-form-mgmt-edit:delete-button",  new FacesMessage(Fehlermeldungen.ADMIN_ADMIN_EMAIL_WIRD_BEREITS_VON_EINEM_ANDEREN_ADMIN_BENUTZT.toString()));
				return null;
			}
		}
		
		adminDao.save(admin);
		return "admins?faces-redirect=true";
	}
	
	public String loeschen() {
		if(admin.getId().equals(userSessionHandler.getAdmin().getId())) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("admin-form-mgmt-edit:delete-button",  new FacesMessage(Fehlermeldungen.ADMIN_ADMIN_LOESCHEN_NICHT_MOEGLICH_DA_AKTUELLER_BENUTZER_IN_SESSION.toString()));
			return null;
		}
		
		
		if(!adminDao.delete(admin)) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("admin-form-mgmt-edit:delete-button",  new FacesMessage("fehler :("));
			return null;
		}
		
		return "admins?faces-redirect=true";
	}
	
	public UserSessionHandler getUserSessionHandler() {
		return userSessionHandler;
	}

	public void setUserSessionHandler(UserSessionHandler userSessionHandler) {
		this.userSessionHandler = userSessionHandler;
	}
	
	public AdminColumnsFuerOrder getOrderByColumnS() {
		return orderByColumnS;
	}

	public OrderTypes getOrderByType() {
		return orderByType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Admin getAdminNeu() {
		return adminNeu;
	}

	public void setAdminNeu(Admin adminNeu) {
		this.adminNeu = adminNeu;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	public OrderByDienst getOrderByDienst() {
		return orderByDienst;
	}
	
}
