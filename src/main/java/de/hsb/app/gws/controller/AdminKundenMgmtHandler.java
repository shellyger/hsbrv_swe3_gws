package de.hsb.app.gws.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.KundeDao;
import de.hsb.app.gws.misc.Fehlermeldungen;
import de.hsb.app.gws.misc.ResponseHelper;
import de.hsb.app.gws.model.Kunde;

@ManagedBean
@Named("adminKundenMgmtHandler")
@ViewScoped
public class AdminKundenMgmtHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{param.id}") // Parameter setzen
	private Long id;
		
	private Kunde kunde;
	
	@Inject
	KundeDao kundeDao;
	
	public void initAdminKundeMgmtPage() throws IOException {
		if(id == null) {
			ResponseHelper.redirectTo("kunden.xhtml");
		}
		
		kunde = kundeDao.findById(id);
		
		if(kunde == null) {
			ResponseHelper.redirectTo("kunden.xhtml");
		}
	}
	
	public String speichereAenderungen() {
		Kunde kundeEmailSuche = kundeDao.findByEmail(kunde.getEmail());
		
		if(kundeEmailSuche != null) {
			if(!kundeEmailSuche.getId().equals(kunde.getId())) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.addMessage("kunde-form-mgmt-edit:submit-button",  new FacesMessage(Fehlermeldungen.KUNDE_REGISTRIERUNG_EMAIL_WIRD_SCHON_BENUTZT.toString()));
				return null;
			}
		}
		
		kundeDao.save(kunde);
		return "kunden?faces-redirect=true";
	}

	public String loescheKunden() {
		if(!kundeDao.delete(kunde)) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("kunde-form-mgmt-edit:submit-button",  new FacesMessage(Fehlermeldungen.FORM_ACTION_AUSFUEHREN_FEHLGESCHLAGEN_PLEASE_RELOAD_AND_RETRY.toString()));
			return null;
		}
		
		return "kunden?faces-redirect=true";
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}

}
