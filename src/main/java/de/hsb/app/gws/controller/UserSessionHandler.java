package de.hsb.app.gws.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import de.hsb.app.gws.model.Admin;
import de.hsb.app.gws.model.Kunde;

@Named
@SessionScoped
public class UserSessionHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	private Kunde kunde;
	private Admin admin;
	
	public Kunde getKunde() {
		return kunde;
	}
	
	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}
	
	public Admin getAdmin() {
		return admin;
	}
	
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}	
	
}
