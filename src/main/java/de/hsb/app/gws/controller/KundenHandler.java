package de.hsb.app.gws.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.KundeDao;
import de.hsb.app.gws.dienste.OrderByDienst;
import de.hsb.app.gws.misc.KundeColumnsFuerOrder;
import de.hsb.app.gws.misc.OrderTypes;
import de.hsb.app.gws.model.Kunde;

@ManagedBean()
@Named("kundenHandler")
@RequestScoped
public class KundenHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	KundeDao kundeDao;
	
	private List<Kunde> kunden;
			
	private String orderBy;
	
	private String orderColumn;
	
	@Inject
	OrderByDienst orderByDienst;

	private KundeColumnsFuerOrder orderByColumnS;
	private OrderTypes orderByType;
	
	public void initKundenListe() {
		if(orderBy == null) {
			orderBy = OrderTypes.DESC.name();
		}
		
		orderByColumnS = orderByDienst.findeKundeColumnFuerOrderEnumByString(orderColumn);
		orderByType = orderByDienst.findeOrderTypeByString(orderBy);
		
		if(orderByColumnS == null || orderByType == null) {
			kunden = kundeDao.getAll();
		} else {
			kunden = kundeDao.getAllWithOrder(orderByColumnS, orderByType);
		}
		
	}
	
	public KundeColumnsFuerOrder getOrderByColumnS() {
		return orderByColumnS;
	}

	public OrderTypes getOrderByType() {
		return orderByType;
	}
	
	public OrderByDienst getOrderByDienst() {
		return orderByDienst;
	}

	public List<Kunde> getKunden() {
		return kunden;
	}

	public void setKunden(List<Kunde> kunden) {
		this.kunden = kunden;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	
}
