package de.hsb.app.gws.controller;




import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.hsb.app.gws.dao.ProduktDao;
import de.hsb.app.gws.misc.IndexProduktSortierAuswahl;
import de.hsb.app.gws.model.Produkt;

@ManagedBean
@Named("produkteHandler")
@ViewScoped
public class ProdukteHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProduktDao produktDao;
	
	private List<Produkt> produkteNichtGesperrt;
	
	private IndexProduktSortierAuswahl sortierung;
	
	@PostConstruct
	public void init() {
		sortierung = IndexProduktSortierAuswahl.NEUSTE;
		sortierungAendern(); // Damit sparen wir uns den extra aufruf vom dao
	}
	
	public void sortierungAendern() {
		switch (sortierung) {
		case NAME_ASC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByNameAsc();
			break;
		case NAME_DESC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByNameDesc();
			break;
		case HERSTELLER_ASC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByHerstellerNameAsc();
			break;
		case HERSTELLER_DESC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByHerstellerNameDesc();
			break;
		case PREIS_ASC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByPreisAsc();
			break;
		case PREIS_DESC:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrtOrderByPreisDesc();
			break;
		default:
			produkteNichtGesperrt = produktDao.getAllNichtGesperrt();
		}
	}

	public IndexProduktSortierAuswahl getSortierung() {
		return sortierung;
	}

	public void setSortierung(IndexProduktSortierAuswahl sortierung) {
		this.sortierung = sortierung;
	}

	public List<Produkt> getProdukteNichtGesperrt() {
		return produkteNichtGesperrt;
	}

	public void setProdukteNichtGesperrt(List<Produkt> produkteNichtGesperrt) {
		this.produkteNichtGesperrt = produkteNichtGesperrt;
	}
	
}
