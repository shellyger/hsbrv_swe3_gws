package de.hsb.app.gws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Entity Bestellposition
 * 
 * Wir speichern die Referenz auf das Produkt als auch die Attribute "produkt_name", "produkt_preis".
 * Der Grund ist, dass sich der Name oder Preis nach der Bestellung �ndern kann und somit die Bestellung "verf�lscht" ist.
 * Oder das Produkt wird gel�scht nach der Bestellung.
 * 
 * @author mzudse
 */

@NamedQuery(name="SelectBestellpositionenByProduktId", query="SELECT bp from Bestellposition bp WHERE bp.produkt_id_a =:produkt_id")

@Entity
public class Bestellposition {
	
	@Id
	@GeneratedValue
	private Long id;
		
	@ManyToOne
	private Produkt produkt;
	
	@ManyToOne
	@NotNull
	private Bestellung bestellung;

	@NotNull
	private Long produkt_id_a;
	
	private String produkt_name;
	
	@Column(name = "preis", precision=8, scale=2)
	private double produkt_preis;
	
	@Min(1)
	private int anzahl;
	
	public Bestellposition() {
		bestellung = null;
		produkt = null;
		produkt_id_a = null;
		produkt_name = "";
		produkt_preis = 0;
		anzahl = 1;
		produkt_id_a = null;
	}

	public Bestellposition(Bestellung bestellung, Produkt produkt, Long produkt_id, String produkt_name, double produkt_preis, @Min(1) int anzahl) {
		super();
		this.bestellung = bestellung;
		this.produkt = produkt;
		this.produkt_id_a = produkt_id;
		this.produkt_name = produkt_name;
		this.produkt_preis = produkt_preis;
		this.anzahl = anzahl;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bestellung getBestellung() {
		return bestellung;
	}

	public void setBestellung(Bestellung bestellung) {
		this.bestellung = bestellung;
	}
	
	public Long getProdukt_id_a() {
		return produkt_id_a;
	}

	public void setProdukt_id_a(Long produkt_id) {
		this.produkt_id_a = produkt_id;
	}

	public String getProdukt_name() {
		return produkt_name;
	}

	public void setProdukt_name(String produkt_name) {
		this.produkt_name = produkt_name;
	}

	public double getProdukt_preis() {
		return produkt_preis;
	}

	public void setProdukt_preis(double produkt_preis) {
		this.produkt_preis = produkt_preis;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	
}
