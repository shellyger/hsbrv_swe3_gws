package de.hsb.app.gws.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.validation.constraints.NotBlank;

@NamedQuery(name="SelectKunden", query="SELECT k from Kunde k order by k.id DESC")

@NamedQuery(name="SelectKundenOrderByIdAsc", query="SELECT k from Kunde k order by k.id ASC")
@NamedQuery(name="SelectKundenOrderByIdDesc", query="SELECT k from Kunde k order by k.id DESC")
@NamedQuery(name="SelectKundenOrderByVornameAsc", query="SELECT k from Kunde k order by k.vorname ASC")
@NamedQuery(name="SelectKundenOrderByVornameDesc", query="SELECT k from Kunde k order by k.vorname DESC")
@NamedQuery(name="SelectKundenOrderByNachnameAsc", query="SELECT k from Kunde k order by k.nachname ASC")
@NamedQuery(name="SelectKundenOrderByNachnameDesc", query="SELECT k from Kunde k order by k.nachname DESC")
@NamedQuery(name="SelectKundenOrderByEmailAsc", query="SELECT k from Kunde k order by k.email ASC")
@NamedQuery(name="SelectKundenOrderByEmailDesc", query="SELECT k from Kunde k order by k.email DESC")

@NamedQuery(name="SelectKundeByEmail", query="SELECT k from Kunde k WHERE k.email =:email")
            
@Entity
public class Kunde {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotBlank
	private String vorname;
	
	@NotBlank
	private String nachname;
		
	@Column(name="email", unique=true)
	private String email;
	
	@NotBlank
	private String passwort;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "kunde", cascade = CascadeType.ALL)
	private List<Bestellung> bestellungen;
	
	public Kunde() {
		this.vorname = "";
		this.nachname = "";
		this.email = "";
		this.passwort = "";
	}
	
	public Kunde(String vorname, String nachname, String email, String passwort) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.passwort = passwort;
	}
	
	public Long getId() {
		return id;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public List<Bestellung> getBestellungen() {
		return bestellungen;
	}

	public void setBestellungen(List<Bestellung> bestellungen) {
		this.bestellungen = bestellungen;
	}
	
	@PreRemove
	private void preRemove() {
	   for (Bestellung bestellung: bestellungen) {
	      bestellung.setKunde(null);
	   }
	   
	   bestellungen = null;
	}

}
