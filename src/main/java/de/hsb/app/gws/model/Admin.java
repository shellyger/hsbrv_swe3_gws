package de.hsb.app.gws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotBlank;

@NamedQuery(name="SelectAdmins", query="SELECT a from Admin a order by a.id DESC")

@NamedQuery(name="SelectAdminsOrderByIdAsc", query="SELECT a from Admin a order by a.id ASC")
@NamedQuery(name="SelectAdminsOrderByIdDesc", query="SELECT a from Admin a order by a.id DESC")
@NamedQuery(name="SelectAdminsOrderByVornameAsc", query="SELECT a from Admin a order by a.vorname ASC")
@NamedQuery(name="SelectAdminsOrderByVornameDesc", query="SELECT a from Admin a order by a.vorname DESC")
@NamedQuery(name="SelectAdminsOrderByNachnameAsc", query="SELECT a from Admin a order by a.nachname ASC")
@NamedQuery(name="SelectAdminsOrderByNachnameDesc", query="SELECT a from Admin a order by a.nachname DESC")
@NamedQuery(name="SelectAdminsOrderByEmailAsc", query="SELECT a from Admin a order by a.email ASC")
@NamedQuery(name="SelectAdminsOrderByEmailDesc", query="SELECT a from Admin a order by a.email DESC")

@NamedQuery(name="SelectAdminByEmail", query="SELECT a from Admin a WHERE a.email =:email")

@Entity
public class Admin {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotBlank
	@Column(name="email", unique=true)
	private String email;
	
	@NotBlank
	private String passwort;
	
	@NotBlank
	private String vorname;
	
	@NotBlank
	private String nachname;
	
	public Admin() {
		email = "";
		passwort = "";
		vorname = "";
		nachname = "";
	}
	
	public Admin(String vorname, String nachname, String email, String passwort) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.email = email;
		this.passwort = passwort;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
}
