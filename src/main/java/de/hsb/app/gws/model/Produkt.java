package de.hsb.app.gws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@NamedQuery(name="SelectProdukte", query="SELECT p from Produkt p order by p.id DESC")

@NamedQuery(name="SelectProdukteOrderByIdAsc", query="SELECT p from Produkt p order by p.id ASC")
@NamedQuery(name="SelectProdukteOrderByIdDesc", query="SELECT p from Produkt p order by p.id DESC")
@NamedQuery(name="SelectProdukteOrderByNameAsc", query="SELECT p from Produkt p order by p.name ASC")
@NamedQuery(name="SelectProdukteOrderByNameDesc", query="SELECT p from Produkt p order by p.name DESC")
@NamedQuery(name="SelectProdukteOrderByPreisAsc", query="SELECT p from Produkt p order by p.preis ASC")
@NamedQuery(name="SelectProdukteOrderByPreisDesc", query="SELECT p from Produkt p order by p.preis DESC")
@NamedQuery(name="SelectProdukteOrderByHerstellerAsc", query="SELECT p from Produkt p order by p.hersteller.name ASC")
@NamedQuery(name="SelectProdukteOrderByHerstellerDesc", query="SELECT p from Produkt p order by p.hersteller.name DESC")

@NamedQuery(name="SelectProdukteWhereGesperrt", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.id DESC")

@NamedQuery(name="SelectProdukteWhereGesperrtOrderByNameAsc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.name ASC")
@NamedQuery(name="SelectProdukteWhereGesperrtOrderByNameDesc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.name DESC")
@NamedQuery(name="SelectProdukteWhereGesperrtOrderByPreisAsc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.preis ASC")
@NamedQuery(name="SelectProdukteWhereGesperrtOrderByPreisDesc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.preis DESC")
@NamedQuery(name="SelectProdukteWhereGesperrtOrderByHerstellerNameAsc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.hersteller.name ASC")
@NamedQuery(name="SelectProdukteWhereGesperrtOrderByHerstellerNameDesc", query="SELECT p from Produkt p WHERE p.gesperrt =:gesperrt order by p.hersteller.name DESC")

@Entity
public class Produkt {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String beschreibung;
	private boolean gesperrt;
	private String bild;
	
	@Column(name = "preis", precision=8, scale=2)
	private double preis;
	
	@ManyToOne
	@NotNull
	private Hersteller hersteller;
	
	public Produkt() {
		this.name = "";
		this.beschreibung = "";
		this.preis = 0.0;
		this.gesperrt = false;
		this.hersteller = null;
		this.bild = "";
	}
	
	public Produkt(String name, String beschreibung, double preis, boolean gesperrt, Hersteller hersteller, String bild) {
		super();
		this.name = name;
		this.beschreibung = beschreibung;
		this.preis = preis;
		this.gesperrt = gesperrt;
		this.hersteller = hersteller;
		this.bild = bild;
	}
	
	public Hersteller getHersteller() {
		return hersteller;
	}

	public void setHersteller(Hersteller hersteller) {
		this.hersteller = hersteller;
	}

	public boolean isGesperrt() {
		return gesperrt;
	}

	public void setGesperrt(boolean gesperrt) {
		this.gesperrt = gesperrt;
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	
	public double getPreis() {
		return preis;
	}
	
	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getBild() {
		return bild;
	}

	public void setBild(String bild) {
		this.bild = bild;
	}

}
