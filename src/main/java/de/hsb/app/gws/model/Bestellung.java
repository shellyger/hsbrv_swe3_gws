package de.hsb.app.gws.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import de.hsb.app.gws.misc.BestellungStatus;

@NamedQuery(name="SelectBestellungen", query="SELECT b from Bestellung b order by b.id DESC")
@NamedQuery(name="SelectBestellungenOrderByIdAsc", query="SELECT b from Bestellung b order by b.id ASC")
@NamedQuery(name="SelectBestellungenOrderByIdDesc", query="SELECT b from Bestellung b order by b.id DESC")
@NamedQuery(name="SelectBestellungenOrderByLiefer_adresse_nameAsc", query="SELECT b from Bestellung b order by b.liefer_adresse_name ASC")
@NamedQuery(name="SelectBestellungenOrderByLiefer_adresse_nameDesc", query="SELECT b from Bestellung b order by b.liefer_adresse_name DESC")
@NamedQuery(name="SelectBestellungenOrderByRechnungs_adresse_nameAsc", query="SELECT b from Bestellung b order by b.rechnungs_adresse_name ASC")
@NamedQuery(name="SelectBestellungenOrderByRechnungs_adresse_nameDesc", query="SELECT b from Bestellung b order by b.rechnungs_adresse_name DESC")
@NamedQuery(name="SelectBestellungenOrderByErstellt_zeitAsc", query="SELECT b from Bestellung b order by b.erstellt_zeit ASC")
@NamedQuery(name="SelectBestellungenOrderByErstellt_zeitDesc", query="SELECT b from Bestellung b order by b.erstellt_zeit DESC")
@NamedQuery(name="SelectBestellungenOrderByStatusAsc", query="SELECT b from Bestellung b order by b.status ASC")
@NamedQuery(name="SelectBestellungenOrderByStatusDesc", query="SELECT b from Bestellung b order by b.status DESC")

@Entity
public class Bestellung {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Kunde kunde;
	
	@NotBlank
	private String liefer_adresse_name;

	@NotBlank
	private String liefer_adresse_strasse;
	
	@NotBlank
	private String liefer_adresse_plz;
	
	@NotBlank
	private String liefer_adresse_stadt;
	
	@NotBlank
	private String rechnungs_adresse_name;

	@NotBlank
	private String rechnungs_adresse_strasse;
	
	@NotBlank
	private String rechnungs_adresse_plz;
	
	@NotBlank
	private String rechnungs_adresse_stadt;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "bestellung", cascade = CascadeType.ALL)
	private List<Bestellposition> bestellpositionen;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date erstellt_zeit;
	
	@Enumerated(EnumType.STRING)
	private BestellungStatus status;
	
	public Bestellung() {
		kunde = null;
		liefer_adresse_name = "";
		liefer_adresse_strasse = "";
		liefer_adresse_plz = "";
		liefer_adresse_stadt = "";
		rechnungs_adresse_name = "";
		rechnungs_adresse_strasse = "";
		rechnungs_adresse_plz = "";
		rechnungs_adresse_stadt = "";
		status = BestellungStatus.OFFEN;
	}
	
	public Bestellung(Kunde kunde, @NotBlank String lieferAdresseName, @NotBlank String lieferAdresseStraße,
			@NotBlank String lieferAdressePlz, @NotBlank String lieferAdresseStadt,
			@NotBlank String rechnungsAdresseName, @NotBlank String rechnungsAdresseStraße,
			@NotBlank String rechnungsAdressePlz, @NotBlank String rechnungsAdresseStadt, BestellungStatus status) {
		super();
		this.kunde = kunde;
		this.liefer_adresse_name = lieferAdresseName;
		this.liefer_adresse_strasse = lieferAdresseStraße;
		this.liefer_adresse_plz = lieferAdressePlz;
		this.liefer_adresse_stadt = lieferAdresseStadt;
		this.rechnungs_adresse_name = rechnungsAdresseName;
		this.rechnungs_adresse_strasse = rechnungsAdresseStraße;
		this.rechnungs_adresse_plz = rechnungsAdressePlz;
		this.rechnungs_adresse_stadt = rechnungsAdresseStadt;
		this.status = status;
	}

	@PrePersist
	protected void onCreate() {
		erstellt_zeit = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}

	public String getLiefer_adresse_name() {
		return liefer_adresse_name;
	}

	public void setLiefer_adresse_name(String liefer_adresse_name) {
		this.liefer_adresse_name = liefer_adresse_name;
	}

	public String getLiefer_adresse_strasse() {
		return liefer_adresse_strasse;
	}

	public void setLiefer_adresse_strasse(String liefer_adresse_strasse) {
		this.liefer_adresse_strasse = liefer_adresse_strasse;
	}

	public String getLiefer_adresse_plz() {
		return liefer_adresse_plz;
	}

	public void setLiefer_adresse_plz(String liefer_adresse_plz) {
		this.liefer_adresse_plz = liefer_adresse_plz;
	}

	public String getLiefer_adresse_stadt() {
		return liefer_adresse_stadt;
	}

	public void setLiefer_adresse_stadt(String liefer_adresse_stadt) {
		this.liefer_adresse_stadt = liefer_adresse_stadt;
	}

	public String getRechnungs_adresse_name() {
		return rechnungs_adresse_name;
	}

	public void setRechnungs_adresse_name(String rechnungs_adresse_name) {
		this.rechnungs_adresse_name = rechnungs_adresse_name;
	}

	public String getRechnungs_adresse_strasse() {
		return rechnungs_adresse_strasse;
	}

	public void setRechnungs_adresse_strasse(String rechnungs_adresse_strasse) {
		this.rechnungs_adresse_strasse = rechnungs_adresse_strasse;
	}

	public String getRechnungs_adresse_plz() {
		return rechnungs_adresse_plz;
	}

	public void setRechnungs_adresse_plz(String rechnungs_adresse_plz) {
		this.rechnungs_adresse_plz = rechnungs_adresse_plz;
	}

	public String getRechnungs_adresse_stadt() {
		return rechnungs_adresse_stadt;
	}

	public void setRechnungs_adresse_stadt(String rechnungs_adresse_stadt) {
		this.rechnungs_adresse_stadt = rechnungs_adresse_stadt;
	}

	public Date getErstellt_zeit() {
		return erstellt_zeit;
	}

	public void setErstellt_zeit(Date erstellt_zeit) {
		this.erstellt_zeit = erstellt_zeit;
	}

	public List<Bestellposition> getBestellpositionen() {
		return bestellpositionen;
	}

	public void setBestellpositionen(List<Bestellposition> bestellpositionen) {
		this.bestellpositionen = bestellpositionen;
	}
	
	public BestellungStatus getStatus() {
		return status;
	}

	public void setStatus(BestellungStatus status) {
		this.status = status;
	}
	
}
