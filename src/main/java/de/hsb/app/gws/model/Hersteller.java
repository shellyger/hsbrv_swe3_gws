package de.hsb.app.gws.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

@NamedQuery(name="SelectHersteller", query="SELECT h from Hersteller h order by h.id DESC")

@NamedQuery(name="SelectHerstellerOrderByIdAsc", query="SELECT h from Hersteller h order by h.id ASC")
@NamedQuery(name="SelectHerstellerOrderByIdDesc", query="SELECT h from Hersteller h order by h.id DESC")
@NamedQuery(name="SelectHerstellerOrderByNameAsc", query="SELECT h from Hersteller h order by h.name ASC")
@NamedQuery(name="SelectHerstellerOrderByNameDesc", query="SELECT h from Hersteller h order by h.name DESC")

@Entity
public class Hersteller {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotBlank
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "hersteller", cascade = CascadeType.ALL)
	private List<Produkt> produkte;
	
	public Hersteller() {
		name = "";
	}
	
	public Hersteller(String name) {
		this.name = name;
	}

	public List<Produkt> getProdukte() {
		return produkte;
	}

	public void setProdukte(List<Produkt> produkte) {
		this.produkte = produkte;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
