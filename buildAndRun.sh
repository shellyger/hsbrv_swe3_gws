#!/bin/sh
mvn clean package && docker build -t de.hsb.app/gws .
docker rm -f gws || true && docker run -d -p 8080:8080 -p 4848:4848 --name gws de.hsb.app/gws 
